<?php
// Template Name: Nossos Lobinhos
?>

<?php get_header(); ?>

    <main>
        <?php get_search_form(); ?>

        
        <section class="wolves-list">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <li>
            <?php if( get_field('foto') ): ?>
                <img class="wolf-image" src="<?php the_field('foto'); ?>" />
            <?php endif; ?>
            
            <div class="wolf-text">
                <div class="wolf-info">
                    <div class="name-age">
                        <h3 class="wolf-name"><?php the_field('nome_do_lobo'); ?></h3>
                        <p class="wolf-age">Idade: <?php the_field('idade'); ?> anos</p>
                    </div>
 
                    <div class="adopt-button">
                    <a href="<?php the_field('button'); ?>">
                        <button >Ver</button>
                    </a>
                    </div>
                </div>
                <p class="wolf-description"><?php the_field('sobre_o_lobo'); ?> </p> 
            </div>
            </li>

        <?php endwhile; else: ?>
                <p>desculpe, o post não segue os critérios escolhidos</p>
        <?php endif; ?>
        </section>

        <nav class="rodape"><?php my_pagination() ?></nav>
    </main>
    
    
    <?php get_footer(); ?>