<?php get_header(); ?>
    <main>
        <div>
        <?php if ( have_posts() ) : ( have_posts() )  ?>
        <li>
            <?php if( get_field('foto') ): ?>
                <img class="wolf-image" src="<?php the_field('foto'); ?>" />
            <?php endif; ?>
            
            <div class="wolf-text">
                <div class="wolf-info">
                    <div class="name-age">
                        <h3 class="wolf-name"><?php the_field('nome_do_lobo'); ?></h3>
                        <p class="wolf-age">Idade: <?php the_field('idade'); ?> anos</p>
                    </div>
                </div>
                <p class="wolf-description"><?php the_field('sobre_o_lobo'); ?> </p> 
            </div>
            </li>
            <?php else: ?>
                <p class="wrong-way">desculpe, o post não segue os critérios escolhidos</p>
            <?php endif; ?>
        </div>

    </main>

<?php get_footer(); ?>