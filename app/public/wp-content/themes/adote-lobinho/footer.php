<footer>
        <div class="wolves-contact">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14700.691828660942!2d-43.140179566471126!3d-22.906990598314565!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ecda45a11%3A0xd356fe0d82ed0f32!2sUFF%20-%20N%C3%BAcleo%20de%20Estudos%20em%20Biomassa%20e%20Gerenciamento%20de%20%C3%81guas!5e0!3m2!1spt-BR!2sbr!4v1632800649561!5m2!1spt-BR!2sbr" width="250" height="175" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <ul>
                <li>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/Contato.svg" alt="Pin de contato">
                    <p>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ, 24210-315</p>
                </li>

                <li>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/Telefone.svg" alt="Telefone">
                    <p>(99) 99999-9999</p>
                </li>

                <li>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/Envelope.svg" alt="E-mail">
                    <p>salve-lobos@lobINhos.com</p>
                </li>

                <li><a href="">Quem somos</a></li>
            </ul>
        </div>

        <div class="wolves-figure">
            <figure>
                <figcaption>Desenvolvido com</figcaption>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/paws.svg" alt="Imagem de patinhas">
            </figure>
        </div>
    </footer>
    
    <script src="../javascript/index.js"></script>
    <?php wp_footer(); ?>
</body>
</html>