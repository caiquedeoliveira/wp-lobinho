<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style-css/index.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style-css/lobos.css">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() ?>/images/Logo.svg" type="image/x-icon">
    <title><?php bloginfo('name') ?></title>
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <nav class="navbar-header">
            <ul class="navbar-list">
            <?php
                $args = array(
                    'menu' => 'navegacao',		
                    'container' => true			
                );					
                wp_nav_menu($args);
            ?>
            </ul>   
        </nav>
    </header>