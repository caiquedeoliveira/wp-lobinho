<?php
// Template Name: Home Page | Adote Lobinho
?>

<?php get_header(); ?>

    <main>
        <section class="main adopt">
            <h1>Adote um Lobinho</h1>
            <p>É claro que o consenso sobre a necessidade de qualificação apresenta tendências no sentido de aprovar a manutenção das regras de conduta normativas.</p>
        </section>

        <section class="main about">
            <h2>Sobre</h2>
            <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
        </section>

        <section class="main values">
            <h2>Valores</h2>
            <ul class="values-list">
                <li>
                    <div class="white-circle">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/Insurance.svg" alt="Proteção">
                    </div>
                    <div>
                        <h3>Proteção</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </li>

                <li>
                    <div class="white-circle">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/care.svg" alt="Carinho">
                    </div>
                    <div>
                        <h3>Carinho</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </li>

                <li>
                    <div class="white-circle">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/Group.svg" alt="Companheirismo">
                    </div>
                    <div>
                        <h3>Companheirismo</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </li>

                <li>
                    <div class="white-circle">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/rescue-dog.svg" alt="Resgate">
                    </div>
                    <div>
                        <h3>Resgate</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </li>
            </ul>
        </section>

        <section class="main wolves">
            <h2>Lobos exemplo</h2>
            <ul class="wolves-list">
            <li>
                <img class="wolf-image" src="https://images.pexels.com/photos/682375/pexels-photo-682375.jpeg" />
            
            <div class="wolf-text">
                <div class="wolf-info">
                    <div class="name-age">
                        <h3 class="wolf-name">Maureen</h3>
                        <p class="wolf-age">Idade: 12 anos</p>
                    </div>
                </div>
                <p class="wolf-description">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p> 
            </div>
            </li>

            <li>
            <img class="wolf-image" src="https://rewildingbritain.imgix.net/images/wolf1_SS.jpg?auto=format&crop=focalpoint&domain=rewildingbritain.imgix.net&fit=crop&fp-x=0.5&fp-y=0.5&h=630&ixlib=php-3.3.1&q=82&w=1200" />
            
            <div class="wolf-text">
                <div class="wolf-info">
                    <div class="name-age">
                        <h3 class="wolf-name">Sidney</h3>
                        <p class="wolf-age">Idade: 30 anos</p>
                    </div>
                </div>
                <p class="wolf-description">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p> 
            </div>
            </li>

            </ul>
        </section>
    </main>

    <?php get_footer(); ?>

